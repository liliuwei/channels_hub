<?php
namespace app\common;

use think\worker\Http as ThinkHttp;
use Workerman\Lib\Timer;
use app\common\Worker;
use think\facade\Log;

class Http extends ThinkHttp
{

    /**
     * 架构函数
     * @access public
     * @param  string $host 监听地址
     * @param  int    $port 监听端口
     * @param  array  $context 参数
     */
    public function __construct($host, $port, $context = [])
    {
        $this->worker = new Worker('http://' . $host . ':' . $port, $context);

        // 设置回调
        foreach ($this->event as $event) {
            if (method_exists($this, $event)) {
                $this->worker->$event = [$this, $event];
            }
        }
    }

    public function onWorkerStart($worker)
    {

        
        parent::onWorkerStart($worker);
        
        
        
    }

    /**
     * 启动
     * @access public
     * @return void
     */
    public function start()
    {

        $channel_service = new ChannelService();
        
        $progress_container = new ProgressController();

        Timer::add(1,function(){
            
            $channel_service = new ChannelService();
            
            $progress_container = new ProgressController();
        });

        Worker::runAll();
    }
}
