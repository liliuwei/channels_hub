<?php

namespace app\common;

use app\common\ChannelServer as Server;
use think\facade\Config;
use think\facade\Log;

class ChannelService extends Http
{

    public function __construct()
    {
        
        Worker::$channelWorker = new Worker();

        Worker::$channelWorker->name = __CLASS__;

        Worker::$channelWorker->onWorkerStart = function(){
            Log::debug('ChannelWorker 运行');
            $channelService = new Server('127.0.0.1',Config::get('channel.channel_server_port'));
            Log::debug('ChannelService 运行,监听'.'127.0.0.1:'.Config::get('channel.channel_server_port'));
            
            $worker = new Worker('tcp://0.0.0.0:8806');
            $worker->listen();
            $worker->run();
        };

        // Worker::$channelWorker->run();

        // return Worker::$channelWorker;

    }
}