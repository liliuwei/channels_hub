-- MySQL dump 10.13  Distrib 5.6.45, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: channel_hub
-- ------------------------------------------------------
-- Server version	5.6.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ch_channel`
--

DROP TABLE IF EXISTS `ch_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_channel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` bigint(20) unsigned NOT NULL COMMENT '类型：1：tcp;2：udp',
  `client_id` bigint(20) unsigned NOT NULL,
  `server_port` bigint(20) unsigned NOT NULL,
  `local_target_ip` varchar(100) DEFAULT NULL,
  `local_target_port` varchar(100) DEFAULT NULL,
  `create_time` bigint(20) unsigned NOT NULL,
  `update_time` bigint(20) unsigned NOT NULL,
  `delete_time` bigint(20) unsigned NOT NULL,
  `status` bigint(20) unsigned NOT NULL COMMENT '0:正常，1：禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_channel`
--

LOCK TABLES `ch_channel` WRITE;
/*!40000 ALTER TABLE `ch_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `ch_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ch_client`
--

DROP TABLE IF EXISTS `ch_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_client` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `create_time` bigint(20) unsigned NOT NULL,
  `update_time` bigint(20) unsigned NOT NULL,
  `delete_time` bigint(20) unsigned NOT NULL,
  `comment` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `status` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '0:正常，1：禁用',
  PRIMARY KEY (`id`),
  KEY `ch_client_key_IDX` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_client`
--

LOCK TABLES `ch_client` WRITE;
/*!40000 ALTER TABLE `ch_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `ch_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ch_http_channel`
--

DROP TABLE IF EXISTS `ch_http_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_http_channel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `local_target_ip` varchar(100) DEFAULT NULL,
  `local_target_port` varchar(100) DEFAULT NULL,
  `create_time` bigint(20) unsigned NOT NULL,
  `update_time` bigint(20) unsigned NOT NULL,
  `delete_time` bigint(20) unsigned NOT NULL,
  `status` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '0:正常，1：禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_http_channel`
--

LOCK TABLES `ch_http_channel` WRITE;
/*!40000 ALTER TABLE `ch_http_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `ch_http_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ch_system_config`
--

DROP TABLE IF EXISTS `ch_system_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_system_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `comment` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ch_system_config_name_IDX` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_system_config`
--

LOCK TABLES `ch_system_config` WRITE;
/*!40000 ALTER TABLE `ch_system_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `ch_system_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'channel_hub'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-15 22:48:53
